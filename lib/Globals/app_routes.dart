import 'package:flutter/material.dart';
import 'package:mvvm_app/Pages/DetailsPage/details_page.dart';
import 'package:mvvm_app/Pages/HomePage/home_page.dart';

class AppRoutes{
  static const String homeRoute = "home";
  static const String detailsRoute = "details";

  static Route onGeneratedRoute(RouteSettings settings) {
    var name = settings.name;
    var arg = settings.arguments;
    switch (settings.name) {
      case homeRoute:
        return MaterialPageRoute(
          builder: (context) => const HomePage(),
        );
      case detailsRoute:
        return MaterialPageRoute(
          builder: (context) => DetailsPage(postId: arg as int),
        );
      default:
        return MaterialPageRoute(
          builder: (context) => const HomePage(),
        );
    }
  }
}
