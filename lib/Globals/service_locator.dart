import 'package:get_it/get_it.dart';
import 'package:mvvm_app/Pages/DetailsPage/details_page_service.dart';
import 'package:mvvm_app/Pages/DetailsPage/details_page_view_model.dart';
import 'package:mvvm_app/Pages/HomePage/home_page_service.dart';
import 'package:mvvm_app/Pages/HomePage/home_page_viewmodel.dart';
import 'package:mvvm_app/Repositories/details_page_service_impl.dart';
import 'package:mvvm_app/Repositories/home_page_dummy_impl.dart';
import 'package:mvvm_app/Repositories/home_page_service_impl.dart';

GetIt serviceLocator = GetIt.instance;

void setupServiceLocator() {
  // services
  serviceLocator.registerLazySingleton<HomePageService>(() => HomePageServiceImpl());
  // serviceLocator.registerLazySingleton<HomePageService>(() => HomePageDummyImpl());
  serviceLocator.registerLazySingleton<DetailsPageService>(() => DetailsPageServiceImpl());

  //ViewModels
  serviceLocator.registerFactory<HomePageViewModel>(() => HomePageViewModel());
  serviceLocator.registerFactory<DetailsPageViewModel>(() => DetailsPageViewModel());
}