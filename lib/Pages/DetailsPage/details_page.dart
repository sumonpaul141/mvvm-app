import 'package:flutter/material.dart';
import 'package:mvvm_app/Globals/service_locator.dart';
import 'package:mvvm_app/Models/post.dart';
import 'package:mvvm_app/Pages/DetailsPage/details_page_view_model.dart';
import 'package:provider/provider.dart';

class DetailsPage extends StatefulWidget {
  final int postId;
  const DetailsPage({Key? key,required this.postId}) : super(key: key);

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  DetailsPageViewModel model = serviceLocator<DetailsPageViewModel>();

  @override
  void initState() {
    model.loadPost(widget.postId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    _buildPost(Post post){
      return Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(post.title, style: Theme.of(context).textTheme.headline6,),
            SizedBox(height: 10,),
            Text(post.body, style: Theme.of(context).textTheme.subtitle1,),
          ],
        ),
      );
    }

    _buildLoading(){
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 50.0),
        child: Center(
          child: Column(
            children: const [
              CircularProgressIndicator(),
              SizedBox(height: 10,),
              Text("Loading.."),
            ],
          ),
        ),
      );
    }

    return ChangeNotifierProvider<DetailsPageViewModel>(
      create: (context) => model,
      child: Consumer<DetailsPageViewModel>(
        builder: (context, model, child) => Scaffold(
          appBar: AppBar(
            title: const Text("Details"),
          ),
          body: model.isLoading ? _buildLoading() : _buildPost(model.post),
        ),
      ),
    );
  }
}
