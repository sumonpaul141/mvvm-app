import 'package:mvvm_app/Models/post.dart';

abstract class DetailsPageService{
  Future<Post> getPostDetails(int id);
}