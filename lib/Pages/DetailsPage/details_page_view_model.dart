import 'package:flutter/material.dart';
import 'package:mvvm_app/Globals/service_locator.dart';
import 'package:mvvm_app/Models/post.dart';
import 'package:mvvm_app/Pages/DetailsPage/details_page_service.dart';

class DetailsPageViewModel extends ChangeNotifier{
  Post _post = Post(userId: 0, id: 0, title: "", body: "");
  DetailsPageService detailsPageService = serviceLocator<DetailsPageService>();
  bool _isLoading = true;

  get post => _post;
  get isLoading => _isLoading;

  loadPost(int id) async {
    _isLoading = true;
    _post = await detailsPageService.getPostDetails(id);
    _isLoading = false;
    notifyListeners();
  }
}