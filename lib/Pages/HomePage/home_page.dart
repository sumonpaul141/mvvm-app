import 'package:flutter/material.dart';
import 'package:mvvm_app/Globals/app_routes.dart';
import 'package:mvvm_app/Globals/service_locator.dart';
import 'package:mvvm_app/Models/post.dart';
import 'package:mvvm_app/Models/user.dart';
import 'package:mvvm_app/Pages/HomePage/home_page_viewmodel.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomePageViewModel model = serviceLocator<HomePageViewModel>();

  @override
  void initState() {
    model.loadData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<HomePageViewModel>(
      create: (context) => model,
      child: Consumer<HomePageViewModel>(
        builder: (context, model, child) => Scaffold(
          appBar: AppBar(
            title: const Text('User list'),
          ),
          body: ListView(
            scrollDirection: Axis.vertical,
            children: [
              const SizedBox(height: 8,),
              model.userList.isNotEmpty
                  ? _buildUserList(model, context)
                  : _buildEmptyList(),
              model.postList.isNotEmpty
                  ? _buildPostList(model, context)
                  : _buildEmptyList(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildUserList(HomePageViewModel viewModel, BuildContext context) {
    return SizedBox(
      height: 100,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: viewModel.userList.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          User user = viewModel.userList[index];
          return Card(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(user.name, style: Theme.of(context).textTheme.headline4,),
                  Text(user.address.city,style: Theme.of(context).textTheme.bodyText1!.copyWith(color : Theme.of(context).primaryColor),),
                ],
              ),
            ),
          );
        },

      ),
    );
  }

  Widget _buildPostList(HomePageViewModel viewModel, BuildContext context) {
    return ReorderableListView.builder(
      itemCount: viewModel.postList.length,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        Post post = viewModel.postList[index];
        return GestureDetector(
          key: Key(index.toString()),
          onTap: () {
            Navigator.of(context)
                .pushNamed(AppRoutes.detailsRoute, arguments: post.id);
          },
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20),
            child: Text(post.title),
          ),
        );
      },
      onReorder: model.onReorder,
    );
  }

  _buildEmptyList() {
    return const Center(
      child: Text("No user available right now"),
    );
  }
}
