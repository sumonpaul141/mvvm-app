import 'package:mvvm_app/Models/post.dart';
import 'package:mvvm_app/Models/user.dart';

abstract class HomePageService{
  Future<List<Post>> getPostList();
  Future<List<User>> getUserList();
}