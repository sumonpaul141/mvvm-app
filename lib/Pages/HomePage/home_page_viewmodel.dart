import 'package:flutter/material.dart';
import 'package:mvvm_app/Globals/service_locator.dart';
import 'package:mvvm_app/Models/post.dart';
import 'package:mvvm_app/Models/user.dart';

import 'home_page_service.dart';

class HomePageViewModel extends ChangeNotifier{
  final HomePageService _homePageService = serviceLocator<HomePageService>();
  List<Post> postList = [];
  List<User> userList = [];

  loadData() async {
    var items = await _homePageService.getPostList();
    var users = await _homePageService.getUserList();
    postList.addAll(items);
    userList.addAll(users);
    notifyListeners();
  }

  onReorder(oldIndex, newIndex){
    if(oldIndex < newIndex){
      newIndex--;
    }
    Post post = postList[oldIndex];
    postList.removeAt(oldIndex);
    postList.insert(newIndex, post);
    notifyListeners();
  }
}