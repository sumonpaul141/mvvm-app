import 'dart:convert';

import 'package:mvvm_app/Models/post.dart';
import 'package:mvvm_app/Pages/DetailsPage/details_page_service.dart';
import 'package:http/http.dart' as http;

class DetailsPageServiceImpl extends DetailsPageService{
  @override
  Future<Post> getPostDetails(int id) async {
    String url = "https://jsonplaceholder.typicode.com/posts/$id";
    var response = await http.get(Uri.parse(url));
    Post post = Post.fromJson(jsonDecode(response.body));
    return post;
  }
}