import 'package:mvvm_app/Models/post.dart';
import 'package:mvvm_app/Models/user.dart';
import 'package:mvvm_app/Pages/HomePage/home_page_service.dart';

class HomePageDummyImpl extends HomePageService{
  @override
  Future<List<Post>> getPostList() async {
    List<Post> posts = [];
    await Future.delayed(const Duration(seconds: 1));
    posts.add(Post(userId: 1, id: 1, title: "Post 1", body: "Body one"));
    posts.add(Post(userId: 2, id: 2, title: "Post 2", body: "Body two"));
    posts.add(Post(userId: 3, id: 3, title: "Post 3", body: "Body three"));
    posts.add(Post(userId: 4, id: 4, title: "Post 4", body: "Body four"));
    return posts;
  }

  @override
  Future<List<User>> getUserList() {
    // TODO: implement getUserList
    throw UnimplementedError();
  }

}