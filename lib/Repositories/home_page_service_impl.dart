import 'package:mvvm_app/Models/post.dart';
import 'package:mvvm_app/Models/user.dart';
import 'package:mvvm_app/Pages/HomePage/home_page_service.dart';
import 'package:http/http.dart' as http;

class HomePageServiceImpl extends HomePageService{

  @override
  Future<List<Post>> getPostList() async {
    String url = "https://jsonplaceholder.typicode.com/posts";
    var response = await http.get(Uri.parse(url));
    return postListFromJson(response.body);
  }

  @override
  Future<List<User>> getUserList() async {
    String url = "https://jsonplaceholder.typicode.com/users";
    var response = await http.get(Uri.parse(url));
    return usersFromJson(response.body);
  }

}