import 'package:flutter/material.dart';
import 'package:mvvm_app/Globals/app_routes.dart';
import 'package:mvvm_app/Globals/service_locator.dart';
import 'package:mvvm_app/Pages/HomePage/home_page.dart';

void main() {
  setupServiceLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      onGenerateRoute: AppRoutes.onGeneratedRoute,
      initialRoute: AppRoutes.homeRoute,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomePage(),
    );
  }
}

